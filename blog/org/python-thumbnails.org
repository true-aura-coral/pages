#+TITLE: Python Thumbnails
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 
* What is this?
This is a script for me to generate video thumbnails for none video
publications. I am way to lazy to create thumbnails so this just
does it automaticly.

#+BEGIN_SRC python
# Python image processing imports. Handling text and other things.
from PIL import Image, ImageDraw, ImageFont
# This if for text wrapping. Using \n isn't supported in PIL text
# stuff.
import textwrap
#+END_SRC

* Open the image
In a directory I store =background.png= here I just open it so I can manipulate it.
#+BEGIN_SRC python
# Image open and draw
image = Image.open("background.png")
draw = ImageDraw.Draw(image)
#+END_SRC
* Add the text
Ask for input, run it through text wrap, specify a font I have in the directory and finally put it on the image.
#+BEGIN_SRC python
# Text
text = input('Text: ')
textwrapped = textwrap.wrap(text, width=30)
dubai = ImageFont.truetype("ReemKufi-Regular.ttf", 100)
draw.text((400,400), '\n'.join(textwrapped), font=dubai, fill="#EE7A2F")
#+END_SRC

* Saving it
I do some manipulation on the text variable so I get a nicer file name.
#+BEGIN_SRC python
filename = text.replace(' ', '-')+".png"
image.save(filename, "PNG")
image.show()
#+END_SRC
* Codeberg link
https://codeberg.org/zortazert/Python-Projects/src/branch/main/thumbnail

#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2022 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
