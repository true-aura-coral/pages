#+TITLE: Pokemon Emulators
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 

I am listing emulators which are open source because those emulators put more time and dedication fighting against nintendo.
* Game Boy, Game Boy Color and Game Boy Advance
These consoles are for playing stuff like:
** Game Boy (First Generation)
- Red
- Green
- Blue
- Yellow
** Game Boy Color (Second Generation)
- Gold & Silver
- Crystal
** Game Boy Advance (Second Generation)
- Ruby & Sapphire
- FireRed & LeafGreen *(First Generation Remake)*
- Emerald
** Emulator
The emulator to use is https://mgba.io/. It is a conctinuation of the Game Boy Advance Emulator a project that's been discontinued. mGBA not only supports gameboy advance games it also supports gameboy/gameboy color (allegedly).
* Nintendo DS
This console is used for playing stuff like:
** Fourth Generation
- Diamond & Pearl
- Platinum
- HeartGold and SoulSilver *(Second Generation Remake)*
** Fifth Generation
- Black & White
- Black 2 & White 2
** Emulator
The emulator to use is http://desmume.org/.
* Nintendo 3DS
This console is used for playing stuff like:
** Sixth Generation
- X & Y
- Omega Ruby & Alpha Sapphire *(Third Generation Remake)*
** Seventh Generation
- Sun & Moon
- Ulrta Sun & Moon
** Emulator
The emulator to use is http://citra-emu.org/.
* Nintendo Switch
This console is used for playing stuff like:
** Eighth Generation
- Let's Go Pikachu & Let's Go Eevee! *(First Generation Remake #2)*
- Sword & Shield
- Brilliant Diamond and Shining Pearl *(Fourth Generation Remake)*
- Legends Arceus *(Fourth Generation Past)*
** Ninth Generation
- Scarlet & Violete
** Emulator
The emulator to use is http://yuzu-emu.org/ or https://ryujinx.org/. I have never used Yuzu but allegedly is more smooth than ryujinx. For me, ryujinx is constantly getting new updates and performance is only getting better.
* Link clickery
The majority of these consoles aren't even sold anymore. Neither are these games. So... clicking some links to get some stuff. archive.org has freely accessable archives for a lot of games released on older consoles.
** GBA (Game Boy Advance)
https://archive.org/download/nointro.gba
** DS
https://archive.org/download/cylums-nintendo-ds-rom-collection/Cylum%27s%20Nintendo%20DS%20ROM%20Collection/
** 3DS
https://archive.org/download/nintendo-3ds-complete-collection
Both 3DS and CIA roms must get decrypted.
1. Install download link from here: https://gbatemp.net/threads/batch-cia-3ds-decryptor-a-simple-batch-file-to-decrypt-cia-3ds.512385/.
2. Extract and place roms you wish to decrypt in the extracted folder.
3. Run the .bat file. When it says "Finished." press any key.
4. You should see a new file which is the previous rom but with -decrypted in the filename. When loading that to citra your rom file should now work.
Video Guide: https://invidio.xamh.de/watch?v=PKPria2BMIQ
** NS (Nintendo Switch)
Getting the switch emulators up and running is not as simple as Open rom, save rom. Make sure to read the documentation of yuzu or ryujinx. Or if your good at following directions of a thousand file manager tabs open on youtube that could work. [[https://github.com/Ryujinx/Ryujinx/wiki/Ryujinx-Setup-%26-Configuration-Guide][Ryujinx Guide]] [[https://yuzu-emu.org/help/quickstart/][Yuzu Guide]]. Now in terms of link clickery, most nintendo switch roms are on 1fichier. Why are they on 1fichier? I do not know. But when using 1fichier directly it's sketchy (you can not use an adblocker) so someone made a downloader. However, they deleted it. Thanks to our good friend archive.org it's still up: [[https://web.archive.org/web/20220405174819/https://github.com/manuGMG/1fichier-dl/releases][1fichier-dl]]. It's very self explanitory to use. Then in order to get 1fichier links I found this [[https://nswgame.com/pokemon-sword-nintendo-switch-nsp-xci-nsz-download-free/][site]]. I believe both ryujinx and yuzu can handle either NSP or XCI files. Or you can risk it for the biscuit with torrenting. These days nintendo is catching link clickers left and right.
* Sources
- https://emulation.gametechwiki.com/index.php/Nintendo_3DS_emulators
- https://wiki.beparanoid.de/wiki/List_of_video_game_console_emulators?lang=en
- https://wiki.beparanoid.de/wiki/List_of_Pok%C3%A9mon_video_games?lang=en
- https://breezewiki.pussthecat.org/nintendo/wiki/List_of_Pok%C3%A9mon_games
- https://www.emulator-zone.com/
- https://www.emulatorgames.net/emulators/
* License
#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2022 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
