#+TITLE: Lumous Trainer Zac Direct
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 

* This is updated to the newest direct (https://www.youtube.com/watch?v=QGblaC1nNJM)
* Continuation of Pokemon Episode Reviews in Pokemon Horizons
*Status*: Doing
There now going to be in bulk format. 4-5 videos posted in a bulk format and not done on a weekley basis. The plan is to now have them come out once a month
** Reason:
Subs come out late around 10-11 PM his time so in order to match his weekley schedule it would take him about 2 hours to edit and post making him go to bed late. Also, he considers himself not just an anime reviews channel. He does other stuff.
** Alternative:
He recommends his friend Niall Kelly as an alternative for more up to date reviews. At the time of writing he has already published a review for episode 40, 3 hours ago. Other options include, Pokeflax and Elite Trainer Mark

** Video Ideas
*** Video Covering episodes 35-40 in pokemon horizons
*** Finishing up Scarlet Violete Nuzlock
*** Other playthroughs that he has done in the past
*** DONE Favorite Episodes of the Pokemon Anime
*** DONE Episodes of the Pokemon Anime
* Pokemon Manga Review Series (Spring 2024)
This video series has been long anticipated and he will start working on it soon.
The first manga review will be of the pokemon journeys manga. It will be compared to the anime.
He is thinking about releasing them once a month

* Gaming Videos
- Challenge Videos
  - Pokemon Scarlet nuzlock playing as ash ketchum
Once a month videos

* Anime (Spring - Summer)
** TODO Pokemon Scarlet & Violete Anime Series with Ash (By the end of this year, waiting for DLC)
- Officially begane production on it
- Plans to do once a month
** DONE Continuation of the Pokemon Sword & Shield Anime Series
CLOSED: [2024-02-17 Sat 11:14]
*Status*: Done
** New Merch celebrating the rebrand of the channel
New ZACTOSHI merch. Helps support the channel.
** Conclusion
There is nice rotation of content "waves" perhaps that will be releasing on a variety of topics. I wonder how zac is going to pull this off. It's not just going to be the main 4 there are other content ideas that zac will get and publish.
* License
#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2022 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
