#+TITLE: Dark mode for codeberg!
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="styles.css" />
#+OPTIONS: html-style:nil toc:nil num:nil

* Why?
I would say that codeberg is my favorite git service compared to github/gitlab.

- [X] Open source
- [X] Reliable
- [X] Website
- [X] Does not requires JS
- [ ] Dark mode....

As you can see codeberg checks all the boxes for me except for one. NO DARK MODE :(

* Thanks to pyerine I found out how.

** Step 1
   Go to https://codeberg.org
 
** Step 2
   Go to the top right corner and click on settings
   
** Step 3
   In your settings go to the account tab
   
** Step 4
   Scroll down to Select default theme.

** Step 5
   Click on the drop down and select arc green.

** Step 6
   Click update theme
   
* BOX check
- [X] Dark mode
