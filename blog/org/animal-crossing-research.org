#+TITLE: Animal Crossing Research
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil 

* Checklist (Illymation idea)
https://www.youtube.com/watch?v=crVPbVQK6jE

- [X] Find the perfect name
  Shrekville???
  Optimailni (Final name)
- [ ] Find the perfect villagers
  Axel is pretty epic
  Pashmina is dumb
  Winnie
  Deli
- [ ] Find the perfect theme for your town

* Day 1 (April 19th 2023)
This is the start of logging my animal crossing activities. Day 1 was the start, I went through the usual animal crossing stuff:
- Chose an island/map
- Chose a name (optimailni)

I chose optimailni as the name because that is optimal in russian, well badly spelled russian. It's oppisite to what animal crossing is with it's repetitive nature of tasks and being very unoptimal. From my understanding it's not like minecraft where you can figure out ways with redstone to farm every item in the game in animal crossing it's about the grind and the friends (digital ones).

In terms of villagers I got axel who says WHONK all the time. I think he is pretty epic on a fitness routine trying to loose some of that weight. Very ironic and funny because elephants weight so much (2 tons).

I also got pashmina who says KIDDERS all the time. Litterally wears a rainbow sweater. Only female on the island currently. I hope there are better females in animal crossing then a deer who has bad memory.

But hey, that's just the start I will learn more about them throughout this adventure. 

* Day ababba (June 11 2023)
Obviously didn't keep track with this but time has passed. Already in june and I think I have played animal crossing everyday. After watching footaferret, I am not time traveling but I am making very slow progress. Animal crossing calms me down after a long and stressful day. I think that's why I keep on coming back to it. I am at the part where 3 new villagers are slowly popping into the island. 2 of the houses I finished crafting their exterior and interior items. The new villagers were deli and whinnie. Deli is some monkey and whinnie is a girl boss horse. We will see how they fare with the OG Axel & Pashmina. I'm also in crippling debt due too my recent upgrade to my house. This loan is double the previous for the house upgrade. I guess it's nook's money making scheme. Most of my bells are getting spent rapidly on nook's cranny and on mable's clothes. Decorating my house is important and looking absolutely aesthetic is worth ignoring my debt.

#+BEGIN_EXPORT html 
<hr> 
<footer> 
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br> 
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2022 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>. 
</footer> 
#+END_EXPORT 
