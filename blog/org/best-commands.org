#+TITLE: COMMANDS
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil

* Powershell
** INTERNET EXPLORER RISES
~(new-object -com "InternetExplorer.application").visible = $true~

* Bash
** Search them files for test
Source: https://stackoverflow.com/questions/16956810/how-can-i-find-all-files-containing-specific-text-string-on-linux#answer-16956844
~grep -Ril "text" $pwd~
* License
#+BEGIN_EXPORT html
<hr>
<footer>
<a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' width='88' height='31' src='../images/cc-by-sa.png' /></a><br>
Unless otherwise noted, all content on this website is Copyright Zortazert 2021-2023 and is licensed under <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>CC BY-SA 4.0</a>.
</footer>
#+END_EXPORT
