#+TITLE: ORG Babel for the win!
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="styles.css" />
#+OPTIONS: toc:nil num:nil

* Why?
I was watching DT's video on [[https://invidio.xamh.de/watch?v=pQe1ul51RM0][org babel]]. I at first didn't really like the concept becauase when he uploaded to gitlab there was no syntax highlighting :( 

Well, when finally trying it out for myself. Apperently babel is built in with emacs by the way. I instinctivly put the language of what I was putting in a source code block. Since I was doing this on my init.el I put lisp. Your actually supposed to put elisp for proper syntax highlighting.

* Result
A absolutely beautiful file that looks awesome on git website and is cool to edit.

* HOW?

* 1.
Take the contents of the file you want to convert to org. Put it in a separate file with the .org extension. If you want to have it in a GIT repository make the file README.org so when opening a folder with the file you will get a preview of it.

* 2.
Go to the top of the file and add this:

#+BEGIN_SRC org
#+PROPERTY: header-args :tangle <file name>
#+END_SRC

Obviously replace *<file name>* with your desired exported file name.

* 3.

Start putting source code blocks using =#+BEGIN_SRC= elisp and =#+END_SRC= all over the place. It would look like this:


=#+BEGIN_SRC elisp=

;; Happy fun code :D

=#+END_SRC=

* 4. 
For the first time you have to do a =C-c C-c= to refresh the document. After that you can just continue pressing: =M x org babel tangle= or =C-c C-v t= when you making changes to your file in org to export it to the actual file.

* My init.el written in org!

I did this on my [[https://codeberg.org/zortazert/Dots/src/branch/main/emacs.d][init.el]] file.

* NOTE:
This probably isn't limited to just coding files. Probably there are other use cases
